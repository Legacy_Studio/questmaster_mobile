import { StackNavigator } from "react-navigation";
// Importing the tab navigator
import tabNavigator from "./TabNavigator";

// Importing routes
import login from "../View/Login";
import home from "../View/Home";
import achievement from "../View/Achievements"
import viewAchievement from "../View/Achievements/viewAchievement"
import editExperience from "../View/ExperienceEditor";
import questViewer from "../View/QuestViewer";
import premiumQuests from "../View/PremiumQuests";


const routes = {
    Login:{ screen: login },
    LoggedInHome:{ screen: tabNavigator },
    Achievements:{ screen: achievement },
    ViewAchievement: {screen: viewAchievement},
    EditExperience: { screen: editExperience },
    QuestViewer: { screen: questViewer },
    PremiumQuests:{ screen: premiumQuests }
}

const config = {
    initialRouteName: "Login"
}

export const routeNames= {
    Login: "Login",
    LoggedInHome: "LoggedInHome",
    Achievements: "Achievements",
    ViewAchievement: "ViewAchievement",
    EditExperience: "EditExperience",
    QuestViewer: "QuestViewer",
    PremiumQuests: "PremiumQuests" 
}

export default StackNavigator(routes, config);