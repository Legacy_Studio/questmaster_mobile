// Tab navigator when logged in
import { TabNavigator } from "react-navigation";

// Views
import home from "../View/Home";
import profile from "../View/Profile";
import questPool from "../View/QuestPool";
import settings from "../View/Settings";
import questLibrary from "../View/QuestLibrary";
// Importing platform
import { Platform } from "react-native";

// Routes
const routes = {
    Home:{ screen: home},
    QuestPool:{screen: questPool},
    QuestLibrary:{ screen: questLibrary },
    Profile:{ screen: profile},
    Settings:{ screen: settings},
}

const config = {
    initialRouteName: "Home",
    swipeEnabled: true,
    lazy: true,
    tabBarPosition: "bottom",
    tabBarOptions:{
        activeBackgroundColor: "#EFD775",
        inactiveBackgroundColor : "#f1c40f",
        showIcon : true,
        showLabel : Platform.OS === "ios" ? true : false,
        labelStyle: {
            fontSize: 12,
            color:"#2a2a2a"
          },
        indicatorStyle:{
            backgroundColor: "#2a2a2a"
        },
        style:{
            backgroundColor: "#f1c40f"
        },
        allowFontScaling:true,
    }
}

export const tabRouteNames ={
    Home: "Home",
    QuestPool: "QuestPool",
    QuestLibrary: "QuestLibrary",
    Profile: "Profile",
    Settings: "Settings"
}

export default TabNavigator(routes,config)