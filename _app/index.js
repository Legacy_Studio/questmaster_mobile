import React from "react";
// Importing the store
import store from "./Store";
import {addNavigationHelpers} from "react-navigation";
import { Provider, connect } from "react-redux";
// Navigator
import Navigator from "./Navigators"
// Store
import reduxStore from "./Store"

class Index extends React.Component {

    render(){
        const { dispatch , navigatorState } = this.props;
        return (
            <Navigator.StackNavigator
                navigation= {
                    addNavigationHelpers({
                        dispatch,
                        state: navigatorState
                    })
                } 
            />
        );
    }
    
}

const mapStateToProps = (state) => {
    return{
        navigatorState: state.StackNavigator
    }
}

const _App = connect(mapStateToProps)(Index);


export default QuestMaster = () => {
    return(
        <Provider store={reduxStore}>
            <_App />
        </Provider>
    );
}