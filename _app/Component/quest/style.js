import {
    StyleSheet
} from "react-native";
// Importing colors
import colors from "../../Styles/colors";

export default {
    title: StyleSheet.create({
        container:{
            flex:1,
            backgroundColor: colors.silver,
            paddingHorizontal:20,
            paddingVertical: 15,
            marginTop:20,
            marginHorizontal: 15,
        },
    }),
    description: StyleSheet.create({
        container:{
            marginVertical:10,
            borderWidth:0,
        },
        categoryText:{
            fontSize:10,
            textAlign:"left"
        },
        rewardText:{
            fontSize:12,
            textAlign:"left"
        }
    }),

}
