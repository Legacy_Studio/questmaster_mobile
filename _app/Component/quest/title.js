import React from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";
// Importing proptypes
import PropTypes from "prop-types";

class Index extends React.Component {

    render(){
        return(
            <View>
                <Text
                    style={{
                        fontSize: this.props.size ? this.props.size : null,
                        textAlign: "center"
                    }}
                    >
                    {this.props.title}
                </Text>
            </View>
        );
    }
}

Index.propTypes = {
    title: PropTypes.string.isRequired,
    size: PropTypes.number,
} 

export default Index;