import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    LayoutAnimation
} from "react-native";
// Importing Interacable View
import Interactable from "react-native-interactable";
// importing prop types
import PropTypes from "prop-types";
// Importing the title
import Title from "./title";
// Importing styles
import styles from "./style"
// React native elements
import { Button } from 'react-native-elements';

class Index extends React.Component{
    constructor(){
        super();
        this.state ={
            isDescription:false,
        }
    }

    componentWillReceiveProps(nextProps){
    }

    // quest settings controller
    questControl(){
        if(this.state.isDescription){
            return(
                <View>
                    <Button
                        large={false}
                        textStyle={{textAlign: 'center'}}
                        title={`Remove`}
                        onPress={()=> alert("Remove Quest")}
                        />
                    {/* <TouchableOpacity
                        onPress={()=>{alert("Remove Quest")}}
                        >
                        <View>
                            <Text>Remove</Text>
                        </View>
                    </TouchableOpacity> */}
                </View>
            );
        }
        else{
            return;
        }
    }

    // Main render function
    render(){
        return(
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={()=>  this.props.ViewQuest(this.props.data)}
                style={styles.title.container}>
                <View>
                    <Title 
                        title={this.props.data.title}
                        size={18}
                    />
                    <Text style={styles.description.categoryText}>Category: {this.props.data.category}</Text>
                    <Text style={styles.description.rewardText}> Reward: {this.props.data.reward} </Text>
                </View>            
            </TouchableOpacity>
        );
    }
}

Index.propTypes ={
    data: PropTypes.object.isRequired,
}

export default Index;