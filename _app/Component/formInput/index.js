import React from "react";
import {
    View,
    Text,
    TextInput
} from "react-native";
// Importing prop types
import PropTypes from "prop-types";
// Importing styles
import styles, { borderBottomColors} from "./style";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Sae } from 'react-native-textinput-effects';


class Index extends React.Component {
    constructor(){
        super();
        this.state={
            focused: false,
            height: 48,
        }
    }

    onFocus(){
        this.setState({
            focused: true,
        })
    }

    onBlur(){
        this.setState({
            focused:false
        })
    }

    updateSize(height){
        this.setState({
            height: height
        })
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.titleContainer}>
                    <Text style={styles.titleText}>{this.props.title}</Text>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                        style={[
                            styles.input,
                            this.props.style ? this.props.style : null,
                            {
                                borderBottomColor:this.state.focused ? borderBottomColors.BLUE : borderBottomColors.DARKER_GRAY
                            }
                        ]}
                        autoCorrect={false}
                        autogrow={true}
                        multiline={true}
                        onFocus={() => this.onFocus()}
                        onBlur={() => this.onBlur()}
                        underlineColorAndroid="rgba(0,0,0,0)"
                        defaultValue={this.props.inputDefaultValue}
                    />
                </View>
            </View>
        )
    }
}

Index.propTypes = {
    title: PropTypes.string.isRequired,
    inputDefaultValue: PropTypes.string.isRequired,
    autogrow: PropTypes.bool,
    style: PropTypes.object
}

export default Index;