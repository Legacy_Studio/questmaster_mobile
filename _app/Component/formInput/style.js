import {
    StyleSheet,
    Dimensions
} from "react-native";
// Screen width and height
const { width, height } = Dimensions.get("window");


export default StyleSheet.create({
    container:{
        marginVertical:10,
    },
    titleContainer:{
        paddingVertical:5,
    },
    titleText:{
        color:"#aaa"
    },
    inputContainer:{
    },
    input:{
        padding:5,
        fontSize:14,
        lineHeight:20,
        borderBottomWidth:0.5,
        borderColor:"#000",
        textAlign:"justify"
    }
})

export const borderBottomColors ={
    DARKER_GRAY: "#1a1a1a",
    BLUE: "#f1c40f"
}