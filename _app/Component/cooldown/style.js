import {
    StyleSheet
} from "react-native";

export default {
    cooldown: StyleSheet.create({
        container:{
            borderWidth:1,
            borderColor:"#2a2a2a",
            backgroundColor:"#dddddd",
            marginHorizontal:60,
            padding:20,
            alignItems:"center"
        },
        cooldownText:{
            fontSize:20,
        }
    })
}