import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    AsyncStorage,
    LayoutAnimation,
    Platform,
    UIManager
} from "react-native";
// Importing styles
import styles from "./style";
// Importing moment
import moment from "moment"
// Importing names
import { namings } from "./lib";
// Importing vector icons
import Icon from "react-native-vector-icons/Ionicons";
// Timer variable
var timer;

class Index extends React.Component {
    constructor(){
        super();
        this.state={
            isCooldownOn:false,
            startedAt: null,
            endAt: null,
            cooldown: {
                hours: null,
                minutes: null,
                seconds: null,
            },
            isCalculateTimeDiff: false,
            isTimerFetched:false,
        }
        // Enabling the Layout animation for android
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentWillMount(){
        if(!this.state.isTimerFetched){
            this.cooldownInitFetch();
        }
    }
    // Fetch more quests
    fetchQuests(){
        let cooldown = moment().add(3, "m");
        this.setState({
            endAt: cooldown,
        },
        () => {
            this.startTheTimer();
            this.setState({
                isCooldownOn:true,
            })
            AsyncStorage.setItem(namings.cooldown, JSON.stringify(cooldown))
        });
    }
    // Initial fetch from the asyncstorage
    cooldownInitFetch(){
        AsyncStorage.getItem(namings.cooldown)
        .then((resp) => {
            let respMoment = JSON.parse(resp);
            console.log("Init fetch: ", moment(respMoment));
            if(resp){
                this.setState({
                    isTimerFetched: true,
                    endAt: moment(respMoment)
                },
                () => { 
                    this.startTheTimer(),
                    LayoutAnimation.easeInEaseOut();
                    this.setState({isCooldownOn: true})
                }
                )
            }
            else{
                this.timerReset();
            }
        });
    }
    // Starting the timer
    startTheTimer(){
        timer = setInterval( () => {
            this.calculateTimeDiff()
            },
        1000
        )
    }
    // Stoping the timer
    stopTheTimer(){
        clearInterval(timer);
        this.setState({
            cooldown:{
                hours: null,
                minutes: null,
                seconds: null
            }
        })
    }
    // Calculates the time difference between right now and started at
    calculateTimeDiff(){
        let duration = moment.duration(this.state.endAt.diff(moment()));
        console.log("Duration Here: ", duration)
        let seconds = duration.asSeconds();
        if( seconds > 0 ){
            LayoutAnimation.easeInEaseOut();
            this.setState({
                cooldown: {
                    hours: duration.hours() < 10 ? "0"+duration.hours() : duration.hours(), 
                    minutes: duration.minutes() < 10 ? "0"+duration.minutes() : duration.minutes(),
                    seconds: duration.seconds() < 10 ? "0"+duration.seconds() : duration.seconds()
                }  
            })
        }
        else{
            this.timerReset();
        }
    }
    // Resetting the timer
    timerReset(){
        LayoutAnimation.easeInEaseOut();
        this.setState({
            endAt: null,
            isCooldownOn:false,
        },
        () => {
            this.stopTheTimer();
            AsyncStorage.removeItem(namings.cooldown);
        }
        )
    }
    // Content Selector
    contentSelector(){
        if(this.state.isCooldownOn){
            return(
                <Text style={styles.cooldown.cooldownText}>
                    { this.state.isCooldownOn 
                        ? 
                        this.state.cooldown.hours+":"+this.state.cooldown.minutes+":"+this.state.cooldown.seconds
                        : 
                        "HH:mm:ss" }
                </Text>
            );
        }
        else{
            return(
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => this.fetchQuests() }
                    >
                    <View>
                        <Icon 
                            name="ios-refresh-outline"
                            size={40}
                            color="#2a2a2a"
                        />
                        {/* <Text style={styles.cooldown.cooldownText}>Fetch Quests</Text> */}
                    </View>
                </TouchableOpacity>
            );
        }
    }
    // Main render function
    render(){
        return(
            <View style={styles.cooldown.container}>
                {this.contentSelector()}
            </View>
        );
    }
}

export default Index;