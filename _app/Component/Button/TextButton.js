import React from "react";
import {
    TouchableOpacity,
    TouchableHighlight,
    View,
    Text,
} from "react-native";
// Importing prop types
import PropTypes from "prop-types";
// Importing styles
import styles from "./style"


class TextButton extends React.Component{
    render(){
        console.log("My props: ", this.props);
        return(
            <TouchableOpacity
                activeOpacity={0.9}
                style={this.props.style}
                onPress={this.props.function}
                >
                <View style={
                    this.props.viewStyle ? this.props.viewStyle : styles.View
                    }>
                    <Text style={ 
                        this.props.textStyle ? this.props.textStyle : styles.Text
                        }>
                            {this.props.text}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
} 

TextButton.propTypes = {
    text: PropTypes.string.isRequired,
    function: PropTypes.func.isRequired,
    style: PropTypes.any,
    textStyle: PropTypes.object,
    viewStyle: PropTypes.object
}

export default TextButton;
    