import {
    StyleSheet,
    Dimensions
} from "react-native";

export default {
    textButton: StyleSheet.create({
        View:{
            justifyContent:"center"
        },
        Text:{
            alignSelf:"center",
            fontSize:14,
            color:"#2a2a2a"
        }
    }),
    imageButton: StyleSheet.create({
        View:{

        },
        Text:{
            
        }
    })
}