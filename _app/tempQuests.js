export default questRotation = [
    {
        id:1,
        title: "Quest 1",
        category: "Entertainment",
        description: "Description fo the quest 1",
        reward: "15xp"
    },
    {
        id:2,
        title: "Quest 2",
        category: "For speed",
        description: "Description fo the quest 2",
        reward: "25xp"
    },
    {
        id:3,
        title: "Quest 3",
        category: "Guardian of the galaxy",
        description: "Description fo the quest 3",
        reward: "50xp"
    },
    {
        id:4,
        title: "Quest 4",
        category: "Hype",
        description: "Description fo the quest 4",
        reward: "30xp"
    },
    {
        id:1,
        title: "Quest 1",
        category: "Entertainment",
        description: "Description fo the quest 1",
        reward: "15xp"
    },
    {
        id:2,
        title: "Quest 2",
        category: "For speed",
        description: "Description fo the quest 2",
        reward: "25xp"
    },
    {
        id:3,
        title: "Quest 3",
        category: "Guardian of the galaxy",
        description: "Description fo the quest 3",
        reward: "50xp"
    },
    {
        id:4,
        title: "Quest 4",
        category: "Hype",
        description: "Description fo the quest 4",
        reward: "30xp"
    },
]