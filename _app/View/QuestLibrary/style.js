// Importing StyleSheet, and Dimensions
import {
    StyleSheet,
    Dimensions
} from "react-native";
// screen width and height
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    gridView: {
      paddingTop: 25,
      flex: 1,
    },
    itemContainer: {
      padding: 10,
      height: 150,
    },
    itemName: {
      fontSize: 16,
      color: '#fff',
      fontWeight: '600',
      textAlign:"center"
    },
    itemCode: {
      fontWeight: '600',
      fontSize: 12,
      color: '#fff',
    },
    categoryIcon:{
      alignSelf:"center",
      marginVertical:10,
    },
  });