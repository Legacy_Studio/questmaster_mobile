import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Platform,
    Dimensions
} from "react-native";
// Importing connect
import { connect } from "react-redux";
// Importing grid View
import GridView from 'react-native-super-grid';
// Importing vector icons
import Icon from "react-native-vector-icons/Ionicons";
// Importing styles
import styles from "./style";
// Importing route names
import { routeNames } from "../../Navigators/StackNavigator";
// Importing Api
import Api from "../../Api/Api";
// Get width and height of the screen
const { width, height } = Dimensions.get("window");

class Index extends React.Component {

    constructor(){
        super();
        this.state ={
            isCategoriesFetched: false,
            categories: [],
        }
    }

    static navigationOptions = {
        //header: null,
        title: "Choose category",
        headerTitleStyle :{
            fontWeight: "normal",
            color:"#2a2a2a"
        },
        headerBackTitle: null,
        tabBarLabel:"Lib",
        tabBarIcon: <View style={{
                            justifyContent: Platform.OS === "ios" ? "center" : "flex-end"
                        }}>
                        <Icon 
                            name="ios-trophy-outline"
                            size={25}
                            color="#2a2a2a"
                            />
                    </View>
    }

    componentWillMount(){
        if(!this.state.isCategoriesFetched){
            Api.setHeader({
                "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTUxMzIyMDk4N30.pU_GmSTNBGKI19vNWvTfPzEPQJpteqk65YtWT1p5yjYwMqf_vMSBjbpOsTWmiQe3MDs6O574Wvktvf75VPzH0A"
            });
            Api.GET("/api/categories")
            .then((resp) => {
                console.log("Response is here: ", resp);
                this.setState({
                    categories: resp,
                    isCategoriesFetched: true
                })
            })
        }
    }

    // Category Button
    category(item){
        return(
            <TouchableOpacity 
                activeOpacity={0.8}
                onPress={()=> this.props.navigation.navigate(routeNames.PremiumQuests, { category: item.category_name })}
                >
                <View style={[styles.itemContainer, { backgroundColor: this.getColor(item.category_name) }]}>
                    <Icon   
                        style={styles.categoryIcon}
                        name={this.getIconName(item.category_name)}
                        size={60}
                        color="#2a2a2a"
                    />
                    <Text style={styles.itemName}>{item.category_name}</Text>
                </View>
            </TouchableOpacity>
        );    
    }

    getColor(name){
        switch(name){
            case "Entertainment":
                return '#1abc9c';
            case "Educational":
                return '#3498db';
            case "Family":
                return '#d35400';
            case "Kids":
                return '#9b59b6';
            case "Language":
                return '#f39c12';
            case "Artistic":
                return '#2980b9';
        }
    }

    getIconName(name){
        switch(name){

            case "Entertainment":
                return "ios-analytics-outline";
            case "Educational":
                return "ios-aperture-outline";
            case "Family":
                return "ios-bonfire-outline";
            case "Kids":
                return "ios-bug-outline";
            case "Language":
                return "ios-bulb-outline";
            case "Artistic":
                return "ios-cafe-outline";
           
        }

    }

    render(){
        // Taken from https://flatuicolors.com/
        const items = [
            { name: 'TURQUOISE', code: '#1abc9c' }, { name: 'EMERALD', code: '#2ecc71' },
            { name: 'PETER RIVER', code: '#3498db' }, { name: 'AMETHYST', code: '#9b59b6' },
            { name: 'WET ASPHALT', code: '#34495e' }, { name: 'GREEN SEA', code: '#16a085' },
            { name: 'NEPHRITIS', code: '#27ae60' }, { name: 'BELIZE HOLE', code: '#2980b9' },
            { name: 'WISTERIA', code: '#8e44ad' }, { name: 'MIDNIGHT BLUE', code: '#2c3e50' },
            { name: 'SUN FLOWER', code: '#f1c40f' }, { name: 'CARROT', code: '#e67e22' },
            { name: 'ALIZARIN', code: '#e74c3c' }, { name: 'CLOUDS', code: '#ecf0f1' },
            { name: 'CONCRETE', code: '#95a5a6' }, { name: 'ORANGE', code: '#f39c12' },
            { name: 'PUMPKIN', code: '#d35400' }, { name: 'POMEGRANATE', code: '#c0392b' },
            { name: 'SILVER', code: '#bdc3c7' }, { name: 'ASBESTOS', code: '#7f8c8d' },
        ];
        return(
                <GridView
                    itemWidth={130}
                    items={this.state.categories}
                    style={styles.gridView}
                    renderItem={item => this.category(item)}
                >
                <View style={{
                        width: width,
                        height: height,
                        position:"absolute",
                        top:0,
                        left:0,
                        backgroundColor:"rgba(42,42,42,0.8)",
                        zIndex:10,
                    }}>
                
                </View>

                </GridView>
        );
    }
}

const mapStateToProps = state => ({
    state: state.QuestLibrary
})

export default connect(mapStateToProps)(Index);