import React from "react";
import {
    View,
    Text
} from "react-native";
// Importing connect
import { connect } from "react-redux";
// Importing styles
import styles from "./style";
// Importing vector icons
import Icon from "react-native-vector-icons/Ionicons";


class Index extends React.Component {

    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.category,
        headerTitleStyle :{
            fontWeight: "normal",
            color:"#2a2a2a"
        },
    })

    // Main render function
    render(){
        console.log("Props: ", this.props)
        const { params } = this.props.navigation.state;
        return(
            <View>
                <Text>Premium Quests</Text>
                <Text>{params.category}</Text>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    state: state.PremiumQuests
})

export default connect(mapStateToProps)(Index);