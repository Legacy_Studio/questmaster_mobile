import React from "react";
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity
} from "react-native";
// Importing styles
import styles from "./style";
// Importing form input
import FormInput from "../../Component/formInput";
// Importing KeyboardAwareScrollView
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// Importing Ionicons 
import Icon from "react-native-vector-icons/Ionicons";

class Index extends React.Component{

    static navigationOptions = ({navigation}) =>({
        headerTitle: null,
        headerTitleStyle:{
            color:"#2a2a2a",
            fontWeight: "normal"
        },
        headerStyle:{
            backgroundColor:"#f1c40f"
        },
        headerLeft: <View>
                        <TouchableOpacity 
                            style={styles.backBtn}
                            onPress={()=> navigation.goBack()}
                            >
                            <View>
                                <Icon 
                                    name="ios-arrow-back-outline"
                                    size={25}
                                    color="#2a2a2a"
                                />
                            </View>
                        </TouchableOpacity>
                    </View>,
        headerRight: <View>
                        <TouchableOpacity
                            style={styles.headerRightBtn}
                            onPress={()=> alert("Save")}>
                            <View style={styles.headerBtnTextWrapper}>
                                <Text style={styles.headerBtnText}>Save</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
    })
    render() {
        const { params } = this.props.navigation.state
        return(
                <View style={styles.container}>
                    <KeyboardAwareScrollView resetScrollToCoords={{ x: 0, y: 0 }}>
                        {/* Form */}
                        <View style={styles.formWrapper}>
                            <View style={styles.form}>
                                {/* Title */}
                                <View>
                                    <FormInput 
                                        title="Title"
                                        inputDefaultValue={params.data.title}
                                    />
                                </View>
                                {/* Company */}
                                <View>
                                    <FormInput 
                                        title="Company"
                                        inputDefaultValue={params.data.company}
                                    />
                                </View>
                                {/* Location */}
                                <View>
                                    <FormInput 
                                        title="Location"
                                        inputDefaultValue={params.data.location}
                                    />
                                </View>
                                {/* Start date and end date currently checkbox*/}
                                {/* description */}
                                <View>
                                    <FormInput 
                                        title="Description"
                                        inputDefaultValue={params.data.description}
                                        autogrow={true}
                                        style={{
                                            minHeight:80,
                                            maxHeight:140
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                        { /* Delete Button */}
                        <View style={styles.removeContainer}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={styles.removeBtn}
                                onPress={()=> {
                                    alert("Remove");
                                }}
                                >
                                <View style={styles.removeBtnTextWrapper}>
                                    <Text style={styles.removeBtnText}>Remove</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
        );
    }
}

export default Index;