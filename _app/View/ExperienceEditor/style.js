import {
    StyleSheet,
    Dimensions
} from "react-native";
import { Avatar } from "react-native-elements";
// Screen width and height
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
        container:{
            flex:1
        },
        backBtn:{
            padding:15,
            paddingHorizontal:20
        },
        formWrapper:{
        },
        form:{
            marginHorizontal:width*0.05,
        },
        removeContainer:{
            flex:1,
            marginTop:30,
            marginHorizontal:width*0.05,
            borderWidth:0.5,
            borderColor:"#7F7200"
        },
        removeBtn:{
            backgroundColor:"#f1c40f",
            paddingVertical:25,
        },
        removeBtnTextWrapper:{
            alignSelf:"center"
        },
        removeBtnText:{
            color:"#2a2a2a"
        },
        headerRightBtn:{
            padding:10,
            marginRight:10,
        },
        headerBtnTextWrapper:{

        },
        headerBtnText:{
            color:"#2a2a2a",
            fontSize:14,
        }
    })