import { 
    StyleSheet,
    Dimensions
} from "react-native";
import colors from "../../Styles/colors"
// Screen width and height
const { width, height } = Dimensions.get("window");
export default style = {
    containers: StyleSheet.create({
        base:{
            flex:1,
            flexDirection:"column"
        },
        content:{
            flex:1,
            backgroundColor:"#fff"
        },
        controller:{
            flex:1,
            backgroundColor: colors.aqua,
        },
        questLog:{
            flexDirection: "column",
            paddingBottom:30,
            borderBottomWidth:1,
            borderColor:"#2a2a2a"
        }
    }),
    title: StyleSheet.create({
        container:{
            paddingVertical: 10,
            paddingHorizontal:width *0.05,
            borderBottomWidth: 1,
            borderColor: "#2a2a2a",
            backgroundColor:"#fff"
        },
        containerExtra:{
        },
        text:{
            fontSize:20,
            color:"#2a2a2a"
        }
    })
}