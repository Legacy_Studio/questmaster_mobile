import React from "react";
import {
    ScrollView,
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Platform,
    UIManager
} from "react-native";
// Importing the connect from the react-redux
import { connect } from "react-redux";
// Importing the FBSDK
import {
    LoginButton,
    AccessToken,
    LoginManager
} from "react-native-fbsdk";
// Importing the styles
import styles from "./style"
// Importing route names
import { routeNames } from "../../Navigators/StackNavigator";
import { tabRouteNames } from "../../Navigators/TabNavigator";
import Header from "../../Styles/Theme/classic/header";
// Importing the quest
import Quest from "../../Component/quest";
// Importing Icon from react-native-vector-icon
import Icon from "react-native-vector-icons/Ionicons";

import Quests from "../../tempQuests"


class Index extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            activeQuestId: null,
        }
        // Enabling the Layout animation for android
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    // Disabling the default header of the stack navigator
    static navigationOptions = ({navigation}) => ({
        title: "Quest Log",
        headerTitleStyle :{
            fontWeight: "normal",
            color:"#2a2a2a"
        },
        tabBarLabel:"Home",
        tabBarIcon: <View style={{
                            justifyContent: Platform.OS === "ios" ? "center" : "flex-end"
                        }}>
                        <Icon 
                            name="ios-home-outline"
                            size={25}
                            color="#2a2a2a"
                            />
                    </View>
    })
    setActiveQuestId = (id) =>{
        if(this.state.activeQuestId !== id){
            this.setState({
                activeQuestId:id
            })
        }
        else{
            this.setState({
                activeQuestId:null,
            })
        }
    }

    // View Quest
    viewQuest(data){
        this.props.navigation.navigate(routeNames.QuestViewer, { quest: data})
    }

    // Render Single quest
    renderQuest(data, index){
        return(
            <Quest 
                key={data.id}
                data={data}
                ViewQuest={(data)=> this.viewQuest(data)}
            />
        );
    }
    render(){
        return(
            <View style={styles.containers.base}>
                <View  style={styles.containers.content}>
                    <ScrollView>
                        {/* Normal quests */}
                        <View>
                            <View style={styles.containers.questLog}>
                                <FlatList 
                                    data={Quests}
                                    keyExtractor={(index,item) => item.id}
                                    renderItem={({item, index})=> this.renderQuest(item,index)}
                                    extraData={this.state}
                                />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    state: state.Home
})

export default connect(mapStateToProps)(Index);