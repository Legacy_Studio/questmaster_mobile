import {
    StyleSheet,
    Dimensions
} from "react-native";
// Screen width and height
const { width, height } = Dimensions.get("window");

export default {
    navigationHeader: StyleSheet.create({
        headerStyle:{
            backgroundColor:"#f1c40f"
        },
        backBtn:{
            padding:15,
            paddingHorizontal:20
        },
    }),
}