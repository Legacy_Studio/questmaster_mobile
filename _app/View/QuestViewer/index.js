import React from "react";
import {
    View,
    Text,
    Button,
    TouchableOpacity
} from "react-native";
// Importing propTypes 
import PropTypes from "prop-types";
// Importing textButton
import TextButton from "../../Component/Button/TextButton";
// Importing vector icons 
import Icon from "react-native-vector-icons/Ionicons";
// Importing styles
import styles from "./style"

class Index extends React.Component{

    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.quest.title,
        headerTitleStyle :{
            color:"#2a2a2a",
            fontWeight:"normal"
        }, 
        headerStyle:styles.navigationHeader.headerStyle,
        headerLeft: <View>
                        <TouchableOpacity 
                            style={styles.navigationHeader.backBtn}
                            onPress={()=> navigation.goBack()}
                            >
                            <View>
                                <Icon 
                                    name="ios-arrow-back-outline"
                                    size={25}
                                    color="#2a2a2a"
                                />
                            </View>
                        </TouchableOpacity>
                    </View>,
    })
    // Main render function
    render(){
        const { quest } = this.props.navigation.state.params;
        return(
            <View>
                <Text>{quest.category}</Text>
                <Text>{quest.description}</Text>
                <Text>{quest.reward}</Text>
                <Button 
                    title="close"
                    onPress={() => alert("close")}
                />
            </View>
        );
    }
}

export default Index;
