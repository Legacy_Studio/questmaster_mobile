import React from "react";
import {
    KeyboardAvoidingView,
    View,
    Text,
    AsyncStorage,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView
} from "react-native";
// Importing the connect from redux
import {connect} from "react-redux";
import Icon from 'react-native-vector-icons/FontAwesome';
// Importing styling
import styles from "./style";
import colors from "../../Styles/colors";
// Importing the routes of the stack navigator
import { routeNames } from "../../Navigators/StackNavigator";
// Importing Stack navigator reducer actions
import navActions from "../../Service/Actions/StackNavigator";
// Facebook login integration
import FBSDK, {
    LoginButton,
    AccessToken,
    LoginManager
} from 'react-native-fbsdk';
// Importing textButton
import TextButton from "../../Component/Button/TextButton";
// Importing KeyboardAwareScrollView
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


class Index extends React.Component{
    constructor(){
        super();
        this.state={
            username: null,
            password: null,
        }
    }
    static navigationOptions = { 
        header: null
    }

    // Component will mount function runs before the render function
    componentWillMount(){
        AccessToken.getCurrentAccessToken()
        .then((data) => {
            // If user is not logged in it will be null
            if(data){
                this.home();
            }
                
        })
    }
    // Getting the user data from the facebook account
    getUserData(token){
        console.log("We ran!");
        return fetch('https://graph.facebook.com/v2.5/me?fields=id,name,email&access_token=' + token)
        .then((resp)=>{
            let respJson = resp.json();
            if(resp.ok){
                return respJson;
            }
        }).catch((err) => console.log("Error: ", err));
    }
    // Navigate to home screen
    home(){
        this.props.dispatch({
            type: navActions.reset,
            routeName: routeNames.LoggedInHome
        })
    }
    // Facebook login function
    _facebookAuth(){
        LoginManager.logInWithReadPermissions(['public_profile','email']).then(
            (result) => {
              if (result.isCancelled) {
                alert('Login cancelled');
              } else {
                  AccessToken.getCurrentAccessToken().then(
                    (data) => {
                        let result = this.getUserData(data.accessToken);
                        result.then((resp) => {
                            console.log("Response is here: ", resp);
                        })
                        if(data)
                            this.home();
                    }
                  )
                
              }
            },
            (error) => {
              alert('Login fail with error: ' + error);
            }
          );
    }

    // Update user name
    updateUsername(text){
        this.setState({
            username: text
        })
    }
    // clear username
    clearUsername(){
        this.setState({
            username: null
        })
    }
    // Update password
    updatePassword(text){
        this.setState({
            password: text
        })
    } 
    // clear password
    clearPassword(){
        this.setState({
            password: null
        })
    }
    render(){
        return(
            <View>
                <Image 
                    source={{
                        uri: "https://lh3.googleusercontent.com/Uwz8Fahk3it3GbDn0oa0rJYQcdQE84M_VwQfug7JcqhFjCCL-sCiUwnVulWbx9ssoiJ8t6miFA=w640-h400-e365"
                    }}
                    style={[styles.background.base, styles.background.bgImage]}
                />
                <View style={[styles.background.base, styles.background.hover]}/>
                <KeyboardAwareScrollView
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    style={styles.login.container}
                    >
                        <View style={styles.logo.container}>
                            <View style={styles.logo.titleWrapper}>
                                <Text style={styles.logo.title}>
                                    Road to Dream
                                </Text>
                            </View>
                            <View style={styles.logo.sloganWrapper}>
                                <Text style={styles.logo.slogan}>Earn experience from everything</Text>
                            </View>
                            {/* <Image 
                                resizeMode="cover"
                                source={{
                                    uri: "https://png.icons8.com/console/Dusk_Wired/56/000000"
                                }}
                                style={styles.logo.image}
                            /> */}
                        </View>
                        <View style={styles.login.form}>
                            {/* Text inputs */}
                            <View style={styles.login.textInputsContainer}>
                                <View style={[styles.login.textInputWrapper, {borderBottomWidth:1,borderColor:"#eee"}]}>
                                    <TextInput 
                                        style={styles.login.textInput}
                                        placeholder="Username"
                                        returnKeyType = {"next"}
                                        //autoFocus={true}
                                        underlineColorAndroid="rgba(255,255,255,0)"
                                        placeholderTextColor="#eee"
                                        onChangeText={(text) => this.updateUsername(text)}
                                        onSubmitEditing={() => {
                                            this.refs.passwordInput.focus();
                                        }}
                                    />
                                </View>
                                <View style={styles.login.textInputWrapper}>
                                    <TextInput 
                                        ref="passwordInput"
                                        style={styles.login.textInput}
                                        secureTextEntry={true}
                                        returnKeyType = {"next"}
                                        //autoFocus={true}
                                        underlineColorAndroid="rgba(255,255,255,0)"
                                        placeholder="Password"
                                        placeholderTextColor="#eee"
                                        onChangeText={(text) => this.updateUsername(text)}
                                    />
                                </View>
                            </View>
                            {/* Login */}
                            <View style={styles.login.loginButtonContainer}>
                                <TextButton 
                                    style={styles.login.loginButton}
                                    function={() => alert("Login")}
                                    text="Login"
                                    textStyle={styles.login.loginButtonText}
                                />
                            </View>
                            <View style={styles.login.forgotPasswordContainer}>
                                <TextButton 
                                    text="Forgot password?"
                                    function={() => alert("Reset password!")}
                                    style={styles.login.forgotPassword}
                                    textStyle={styles.login.forgotPasswordText}
                                />
                            </View>
                            <View style={styles.login.fbLoginContainer}>
                                <Icon.Button 
                                    name="facebook"
                                    backgroundColor={colors.blue}
                                    onPress={()=> { this._facebookAuth() }}
                                    >
                                    <Text style={{color:"#fff"}}> Login with Facebook</Text>
                                </Icon.Button>
                            </View>
                        </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        state: state.Login
    }
}

export default connect(mapStateToProps)(Index);

{/* <View>
                            <Icon.Button 
                                name="facebook"
                                backgroundColor={colors.blue}
                                onPress={()=> { this._facebookAuth() }}
                            >
                                <Text style={{color:"#fff"}}> Login with Facebook</Text>
                            </Icon.Button>
                        </View>
                        <View style={{marginTop: 25}}>
                            <Icon.Button 
                                name="google-plus"
                                backgroundColor={colors.orange}
                                onPress={()=> { Alert.alert("Login with Google!") }}
                            >
                                <Text style={{color:"#fff"}}> Login with Google</Text>
                            </Icon.Button>
                        </View> */}