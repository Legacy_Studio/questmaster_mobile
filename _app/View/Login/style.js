import { 
    StyleSheet,
    Dimensions 
} from "react-native";
// Importing colors
import colors from "../../Styles/colors"

const { width, height} = Dimensions.get("window");

export default styles = {
    background: StyleSheet.create({
        base:{
            position:"absolute",
            top:0,
            left:0,
            zIndex:1,
            width: width,
            height:height,
        },
        bgImage:{
            resizeMode:"cover",
        },
        hover:{
            zIndex:2,
            backgroundColor:"rgba(11,58,54,0.5)"
        }
    }),
    logo: StyleSheet.create({
        container:{
            marginTop:100,
            alignSelf:"center",
            alignItems:"center",
            backgroundColor:"rgba(255,255,255,0)"
        },
        image:{
            width: 100,
            height:100,
        },
        titleWrapper:{
            width:width*0.7,
        },
        title:{
            fontSize:40,
            color:"#eee",
            textAlign:"center"
        },
        sloganWrapper:{
            width:width*0.7,
            padding:5,
            paddingHorizontal:10,
        },
        slogan:{
            color:"#eee",
            fontStyle:"italic",
            textAlign :"center"
        }
    }),
    login: StyleSheet.create({
        container:{
            zIndex:10,
        },
        form:{
            marginTop:50,
        },
        textInputsContainer:{
            backgroundColor:"rgba(38,193,147,0.5)", //#26C193
            paddingVertical:15,
            paddingHorizontal:25
        },
        textInputWrapper:{
            paddingVertical:10,
        },
        textInput:{
            color:"#fff"
        },
        loginButtonContainer:{
            marginTop:20,
            marginHorizontal:25,
        },
        loginButton:{
            alignSelf:"center",
            borderRadius:4,
            minWidth:150,
            maxWidth:250,
            paddingVertical:10,
            paddingHorizontal:30,
            backgroundColor:"#26C193",
            borderColor:"#ddd",
            alignItems:"center"
        },
        loginButtonText:{
            color:"#fff",
        },
        fbLoginContainer:{
            marginTop:100,
            width:200,
            alignSelf:"center",
        },
        forgotPasswordContainer:{
            marginTop:20,
        },
        forgotPassword:{
            alignSelf:"center",
            backgroundColor:"rgba(255,255,255,0)"
        },
        forgotPasswordText:{
            color:"#ccc",
            fontSize:14,
            fontStyle:"italic"
        }
    })
}