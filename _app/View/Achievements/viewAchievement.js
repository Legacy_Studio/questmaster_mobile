import React from "react";
import {
    View,
    Text,
} from "react-native";
// Importing textButton
import TextButton from "../../Component/Button/TextButton"

class Index extends React.Component{

    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.data.name,
        headerTitleStyle :{
            fontWeight: "normal",
            color:"#2a2a2a"
        },
    })

    render(){
        console.log("Props: ", this.props);
        const {params} = this.props.navigation.state;
        console.log("Params: ", params.data)
        return(
            <View>
                {/* Achievement content */}
                <View>
                    {/* Name */}
                    <View>
                        <Text>Name</Text>
                    </View>
                    {/* Date */}
                    <View>
                        <Text>Date</Text>
                    </View>
                    {/* Category */}
                    <View>
                        <Text>Category</Text>
                    </View>
                    {/* Difficulty */}
                    <View>
                        <Text>Difficulty</Text>
                    </View>
                    {/* Description */}
                    <View>
                        <Text>Description</Text>
                    </View>
                    {/* Requirement */}
                    <View>
                        <Text>Requirement</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default Index;
