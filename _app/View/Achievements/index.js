import React from "react";
import {
    View,
    Text,
    FlatList,
    Modal,
    TouchableOpacity
} from "react-native";
// Importing connect from redux
import { connect } from "react-redux";
// Super grid
import GridView from 'react-native-super-grid';
//Importing styles
import styles from "./style"
// Importing vector icons
import Icon from "react-native-vector-icons/Ionicons"
// Importing textButton
import TextButton from "../../Component/Button/TextButton"
// Start rating 
import StarRating from 'react-native-star-rating';
import { routeNames } from "../../Navigators/StackNavigator";

class Index extends React.Component{
    constructor(){
        super();
        this.state={
            modalVisibility: false,
            selectedAchievement:{},
        }
    }

    // Navigation header config
    static navigationOptions = ({navigation}) => ({
        title: "Achievements",
        headerTitleStyle :{
            fontWeight: "normal",
            color:"#2a2a2a"
        },
        headerBackTitle: null
    })

    // View an achievement
    selectAchievement(item){
        // this.setState({
        //     selectedAchievement: item,
        //     modalVisibility:true
        // })
        this.props.navigation.navigate(routeNames.ViewAchievement, {data: item})
    }
    // Close modal
    deselectAchievement(){
        this.setState({
            modalVisibility:false,
            selectedAchievement:{}
        })
    }

    // Render an item
    renderItem(item){
        return(
            <View style={[styles.itemContainer]}>
                <View style={styles.nameContainer}>
                    {/* Achievement name*/}
                    <View style={styles.nameWrapper}>
                        <Text style={styles.nameText}>Name</Text>
                    </View>
                    {/* Achievement category */}
                    <View style={styles.categoryWrapper}>
                        <Text style={styles.categoryText}>2017/12/25</Text>
                    </View>
                </View>
                {/* Achievement difficulty*/}
                <View style={styles.difficultyContainer}>    
                    <View style={ styles.difficultyWrapper}>
                        <Text style={styles.difficultyText}>Difficulty</Text>
                        <StarRating
                            disabled={true}
                            maxStars={5}
                            rating={2.5}
                            starSize={20}
                            emptyStar={'ios-star-outline'}
                            fullStar={'ios-star'}
                            halfStar={'ios-star-half'}
                            iconSet={'Ionicons'}
                            starColor={'#2a2a2a'}
                            emptyStarColor={"#2a2a2a"}
                        />
                    </View>
                </View>
                {/* View control */}
                <View style={styles.previewContainer}>
                    <TouchableOpacity
                        style={styles.previewBtn}
                        onPress={()=> this.selectAchievement(item)}
                        >
                        <View>
                            <Icon 
                                name="ios-eye-outline"
                                size={30}
                                color="#2a2a2a"
                                />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render(){
        const items = [
            { name: 'TURQUOISE', code: '#1abc9c' }, { name: 'EMERALD', code: '#2ecc71' },
            { name: 'PETER RIVER', code: '#3498db' }, { name: 'AMETHYST', code: '#9b59b6' },
            { name: 'WET ASPHALT', code: '#34495e' }, { name: 'GREEN SEA', code: '#16a085' },
            { name: 'NEPHRITIS', code: '#27ae60' }, { name: 'BELIZE HOLE', code: '#2980b9' },
            { name: 'WISTERIA', code: '#8e44ad' }, { name: 'MIDNIGHT BLUE', code: '#2c3e50' },
            { name: 'SUN FLOWER', code: '#f1c40f' }, { name: 'CARROT', code: '#e67e22' },
            { name: 'ALIZARIN', code: '#e74c3c' }, { name: 'CLOUDS', code: '#ecf0f1' },
            { name: 'CONCRETE', code: '#95a5a6' }, { name: 'ORANGE', code: '#f39c12' },
            { name: 'PUMPKIN', code: '#d35400' }, { name: 'POMEGRANATE', code: '#c0392b' },
            { name: 'SILVER', code: '#bdc3c7' }, { name: 'ASBESTOS', code: '#7f8c8d' },
          ];
        return(
            <View>
                <View style={styles.achievementListContainer}>
                    <FlatList 
                        data={items}
                        renderItem={({item, index}) => this.renderItem(item)}
                    />
                </View>
            </View>
           
        );
    }
}

const mapStateToProps = state => ({
    state: state.Achievements
})

export default connect(mapStateToProps)(Index);
