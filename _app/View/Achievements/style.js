import {
    StyleSheet,
    Dimensions
} from "react-native";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    gridView: {
      paddingTop: 25,
      flex: 1,
    },
    itemContainer: {
      flexDirection:"row",
      paddingHorizontal: 10,
      borderBottomWidth:1,
      backgroundColor:"#fff",
      borderBottomColor:"#f1c40f"
    },
    itemName: {
      fontSize: 16,
      color: '#fff',
      fontWeight: '600',
    },
    itemCode: {
      fontWeight: '600',
      fontSize: 12,
      color: '#fff',
    },
    headerContainer:{
      flex:1,
      flexDirection:"row"
    },
    achievementListContainer:{
    },
    nameContainer:{
      flex:5,
      justifyContent:"center"
    },
    nameWrapper:{

    },
    nameText:{
      fontSize:18,
      color:"#2a2a2a"
    },
    categoryWrapper:{

    },
    categoryText:{
      fontSize:12,
      fontStyle:"italic",
      color:"#2a2a2a"
    },
    difficultyContainer:{
      flex:5,
      justifyContent: "center",
      flexDirection:"row",
    },
    difficultyWrapper:{
      flex:1,
      alignItems:"flex-start",
      justifyContent:"center",
    },
    difficultyText:{

    },
    previewContainer:{
      flex:2,
      justifyContent:"center",
    },
    previewBtn:{
      alignItems:"center",
      paddingVertical:20,
    },
    // Modal styling starting from here
    closeBtn:{
      alignItems:"center",
      backgroundColor:"#2196F3",
      padding:10,
    },
    closeBtnText:{
      color:"#fff",
      fontStyle:"italic"
    }
  });
  