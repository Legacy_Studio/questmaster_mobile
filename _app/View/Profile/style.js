import {
    StyleSheet,
    Dimensions
} from "react-native";
import { Avatar } from "react-native-elements";
// Screen width and height
const { width, height } = Dimensions.get("window");
// Colors
import colors from "../../Styles/colors";

const AVATAR_SIZE = 100;
const STICKY_HEADER_HEIGHT = 70;
export default {
    title:{
        container:{
            paddingVertical:10,
            borderBottomWidth:1,
            borderColor:"#2a2a2a"
        },
        text:{
            fontSize:20,
            paddingHorizontal:width*0.05,
            color:"#2a2a2a"
        }
    },
    // Very high level containers
    containers: StyleSheet.create({
        header:{
            flex:1,
            backgroundColor:"#2a2a2a"
        },
        content:{
            flex:9,
            //backgroundColor:"#2196F3"
        }
    }),
    profileHeader: StyleSheet.create({
        container:{
            backgroundColor:"#fff",
            paddingVertical:20,
            borderBottomWidth:1,
            borderColor:"#2a2a2a"
        },
        rightContent:{
            flex:7,
            flexDirection:"column"
        },
        topWrapper:{
            flexDirection:"row",
        },
        bottomWrapper:{
            borderTopWidth:1,
            borderColor:"#2196F3",
            marginTop:10,
            paddingTop:10,
            marginHorizontal:20
        },
        levelLabelWrapper:{
            paddingBottom:10,
            justifyContent:"center",
            alignItems:"center"
        },
        levelLabel:{
            fontSize:20
        },
        imageContainer:{
            flex:3
        },
        imageWrapper:{
            alignSelf:"center"
        },
        image:{
            width:AVATAR_SIZE,
            height: AVATAR_SIZE,
            borderRadius: AVATAR_SIZE/2,
            borderWidth:1,
            borderColor:"#fff"
            //resizeMode:"contain"
        },
        nameContainer:{
            //flex:7,
            justifyContent:"center",
            alignItems:"center"
        },
        nameText:{
            fontSize:22,
            color:"#fff"
        },
        titleContainer:{
        },
        titleWrapper:{
            alignItems:"center"
        },
        title:{
            fontSize:16,
            color:"#fff"
        },
        subtitle:{
            fontSize:12,
            color:"#fff"
        },
        stickyHeader:{
            backgroundColor:"#f1c40f",
            justifyContent:"center",
            height: STICKY_HEADER_HEIGHT,
            paddingLeft:width*0.1,
        },
        stickyHeaderTitle:{
            color:"#2a2a2a",
            fontSize:16,
        },
        stickyHeaderSubtitle:{
            color:"#2a2a2a",
            fontSize:12
        }
    }),
    // About me 
    aboutMe: StyleSheet.create({
        // basic container
        container:{
            //backgroundColor:"#871B1C",
            paddingVertical:20,
            paddingHorizontal: width*0.05,
        },
        // Label starting from here
        labelWrapper:{
            borderBottomWidth:1,
            borderColor:"#2a2a2a",
            paddingVertical:5
        },
        labelText:{
            fontSize:20,
            color:"#2a2a2a",
        },
        // Achievement starting from here
        achievementContainer:{

        },
        //Achievement Label
        achievementLabelWrapper:{
            paddingVertical:10,
        },
        achievementLabel:{
            fontSize:16,
            color:"#2a2a2a"
        },
        // Achievement Point
        achievementPointWrapper:{
            marginLeft:10,
        },
        achievementPoint:{
            color:"#2a2a2a",
            fontStyle:"italic"
        },
        achievementButtonContainer:{
            marginHorizontal:30,
            justifyContent:"center",
            paddingTop:10,
        },
        achievementButton:{
            backgroundColor: colors.main,
            padding:10,
            alignItems:"center"
        },
        achievementButtonText:{
            color:"#2a2a2a"
        },
        summaryContainer:{
            marginTop:20,
            //backgroundColor:"#2196F3"
        },
        summaryText:{
            textAlign:"justify"
        }
    }),
    logOut: StyleSheet.create({
        container:{
            marginVertical:15,
            justifyContent:"center"
        },
        button:{
            alignSelf:"center",
            alignItems:"center",
            backgroundColor: colors.main,
            paddingHorizontal:20,
            paddingVertical:10,
            width:width*0.5
        },
        text:{
            color:"#2a2a2a"
        }
    }),
    experience: StyleSheet.create({
        container:{
            //paddingVertical:20,
        },
        experienceListContainer:{

        },
        experienceWrapper:{
            marginLeft:25,
            paddingVertical:20,
            borderBottomWidth:1,
            borderColor:"#cdcdcd",
        },
        roleWrapper:{

        },
        roleText:{
            fontSize:18,
            fontWeight:"bold",
            color:"#2a2a2a"
        },
        companyWrapper:{

        },
        companyText:{
            fontSize:16,
            color:"#6a6a6a"
        },
        durationWrapper:{

        },
        durationText:{
            fontSize:14
        },
        locationWrapper:{

        },
        locationText:{
            fontSize:12,
        },
        summaryWrapper:{
            marginTop:15,
        },
        summaryText:{
            lineHeight:18,
            fontSize:12
        },
        editWrapper:{
            position:"absolute",
            top:60,
            right:10,
            zIndex:10,
        },
        editBtn:{
            padding:5,
        }
    }),
    interest: StyleSheet.create({
        container:{

        },
        listContainer:{
            padding:10,
        },
        listText:{
            color:"#6a6a6a",
        }
    }),
    consts:{
        AVATAR_SIZE: AVATAR_SIZE,
        STICKY_HEADER_HEIGHT: STICKY_HEADER_HEIGHT,
    },

}