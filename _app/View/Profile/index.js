import React from "react";
import {
    View,
    Text,
    ScrollView,
    TextInput,
    Image,
    FlatList,
    TouchableOpacity,
    Dimensions,
    Modal,
    Platform
} from "react-native"
// Importing connect from react-redux
import { connect } from "react-redux";
// Facebook login integration
import FBSDK, {
    LoginButton,
    AccessToken,
    LoginManager
} from 'react-native-fbsdk';
// Importing the routes of the stack navigator
import { routeNames } from "../../Navigators/StackNavigator";
// Importing Stack navigator reducer actions
import navActions from "../../Service/Actions/StackNavigator";
// Importing react-native-progress
import * as Progress from "react-native-progress";
// Importing styles
import styles from "./style";
// Importing Text Opacity Button
import TextButton from "../../Component/Button/TextButton";
// Importing vector icons
import Icon from "react-native-vector-icons/Ionicons";
// ParallaxScrollView
import ParallaxScrollView from 'react-native-parallax-scroll-view';

//Screen Width and height
const { width, height } = Dimensions.get("window");

const Experience = {
    title: "CEO & Founder",
    company: "Legacy LLC",
    date: "June 18 - present",
    location: "Vancouver, BC, Canada",
    description:"Design, rapid prototyping a mobile game according to the given requirement, develop a necessary plugin, and component on demand. Currently, My main project is a mobile application for Piano teacher and students. The framework, I'm using is the React-native."
}

class Index extends React.Component {

    constructor(){
        super();
        this.state={
            profileHeaderSubtitle: true,
            experienceModal: false,
        }
    }

    static navigationOptions ={
        header: null,
        tabBarLabel:"Profile",
        tabBarIcon: <View style={{
                            justifyContent: Platform.OS === "ios" ? "center" : "flex-end"
                        }}>
                        <Icon 
                            name="ios-contact-outline"
                            size={25}
                            color="#2a2a2a"
                            />
                    </View>
    }

    // Log out the user
    logOut(){
        LoginManager.logOut();
        this.props.dispatch({
            type: navActions.reset,
            routeName: routeNames.Login
        })
    }

    render(){
        console.log("This state: ", this.state)
        return(
            <ParallaxScrollView
                stickyHeaderHeight={ styles.consts.STICKY_HEADER_HEIGHT }
                parallaxHeaderHeight={300}
                renderBackground={() => (
                    <View key="background">
                        <Image source={{uri: "https://www.computing.co.uk/w-images/fde53a6f-c550-4d36-be90-502ba01395ba/0/nanotechnologyoriginal-580x358.jpg",
                                        width: window.width,
                                        height: 300}}/>
                        <View style={{position: 'absolute',
                                    top: 0,
                                    width: window.width,
                                    backgroundColor:"rgba(42,42,42,0.8)",
                                    zIndex:10,
                                    height: 300}}/>
                    </View>
                )}
                renderStickyHeader={() => (
                    <View key="sticky-header" style={styles.profileHeader.stickyHeader}>
                        <Text style={styles.profileHeader.stickyHeaderTitle}>Nominbat Ganbat</Text>
                        <Text style={styles.profileHeader.stickyHeaderSubtitle}>CEO & Founder</Text>
                    </View>
                )}
                renderForeground={() => (
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop:100, }}>
                        <View style={styles.profileHeader.imageContainer}>
                            <View style={styles.profileHeader.imageWrapper}>
                                <Image 
                                    resizeMode="cover"
                                    style={styles.profileHeader.image}
                                    source={{ uri: "https://scontent-nrt1-1.xx.fbcdn.net/v/t31.0-8/19488591_1538714992865455_6713001277813360566_o.jpg?oh=5f34d9caa0ec46ae6cc5b638e2cbe2db&oe=5AA2E360"}}
                                />
                            </View>
                            <View style={styles.profileHeader.nameContainer}>
                                <Text style={styles.profileHeader.nameText}>Nominbat Ganbat</Text>
                            </View>
                            <View style={styles.profileHeader.titleContainer}>
                                <View style={styles.profileHeader.titleWrapper}>
                                    <Text style={styles.profileHeader.title}>
                                        CEO & Founder
                                    </Text>
                                    {
                                        /* showing the company name is optional */
                                        this.state.profileHeaderSubtitle ?
                                            <Text style={styles.profileHeader.subtitle}>
                                                at Legacy LLC
                                            </Text>
                                            :
                                            null
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                )}>
                <View>
                    {/* About me*/}
                    <View style={styles.aboutMe.container}>
                        {/* Title */}
                        <View style={styles.title.container}>
                            <Text style={styles.title.text}>About</Text>
                        </View>
                        <View style={styles.aboutMe.summaryContainer}>
                            <Text style={styles.aboutMe.summaryText}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Quisque sit amet dapibus nisl. Aliquam eu auctor neque, sed dapibus eros. 
                                Proin tincidunt auctor libero vel tempus. Suspendisse ultrices iaculis mi vitae elementum. 
                                Phasellus vitae varius erat. Quisque eu consequat tortor. Sed fermentum tristique fermentum. 
                                Praesent elit nisl, gravida vitae sapien id, egestas gravida enim. 
                                Vestibulum vel leo ullamcorper sapien vestibulum egestas et sed ipsum. 
                                Mauris vehicula suscipit erat gravida rutrum. Sed molestie id velit nec euismod.
                            </Text>
                        </View>
                        {/* Interest */}
                        <View style={styles.interest.container}>
                            <View style={styles.title.container}>
                                <Text style={styles.title.text}>Interest</Text>
                            </View>
                            <View style={styles.interest.listContainer}>
                                <Text style={styles.interest.listText}> 
                                    Extreme sport, Music, Video game, Design, Illustration
                                </Text>
                            </View>
                        </View>
                        {/* Achievements */}
                        <View style={styles.aboutMe.achievementContainer}>
                            <View style={styles.aboutMe.achievementLabelWrapper}>
                                <Text style={styles.aboutMe.achievementLabel}>Achievement</Text>
                            </View>
                            <View style={styles.aboutMe.achievementPointWrapper}>
                                <Text style={styles.aboutMe.achievementPoint}>Total Achievement point: 1340</Text>
                            </View> 
                            <View style={styles.aboutMe.achievementButtonContainer}>
                                <TextButton
                                    style={styles.aboutMe.achievementButton}
                                    function={() => this.props.navigation.navigate(routeNames.Achievements)}//this.props.navigation.navigate(routeNames.Achievements)}
                                    text="Achievement List"
                                    textStyle={styles.aboutMe.achievementButtonText}
                                />
                            </View>
                        </View>
                        {/* Experience */}
                        <View style={styles.experience.container}>
                            <View style={styles.title.container}>
                                <Text style={styles.title.text}>Experience</Text>
                            </View>
                            <View style={styles.experience.experienceListContainer}>
                                <View style={styles.experience.experienceWrapper}>
                                    {/*  Role */}
                                    <View style={styles.experience.roleWrapper}>
                                        <Text style={styles.experience.roleText}>{Experience.title}</Text>
                                    </View>
                                    {/*  Company name */}
                                    <View style={styles.experience.companyWrapper}>
                                        <Text style={styles.experience.companyText}>{Experience.company}</Text>
                                    </View>
                                    {/*  Duration */}
                                    <View style={styles.experience.durationWrapper}>
                                        <Text style={styles.experience.durationText}>{Experience.date}</Text>
                                    </View>
                                    {/*  location */}
                                    <View style={styles.experience.locationWrapper}>
                                        <Text style={styles.experience.locationText}>{Experience.location}</Text>
                                    </View>
                                    {/*  Summary */}
                                    <View style={styles.experience.summaryWrapper}>
                                        <Text style={styles.experience.summaryText}>
                                            {Experience.description}    
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.experience.editWrapper}>
                                <TouchableOpacity
                                    style={styles.experience.editBtn}
                                    onPress={() => this.props.navigation.navigate(routeNames.EditExperience, {data: Experience})}
                                    >
                                    <View>
                                        <Icon 
                                            name="ios-color-wand-outline"
                                            size={30}
                                            color="#cecece"
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                    </View>
                </View>
            </ParallaxScrollView>
        );
    }
}

const mapStateToProps = state => ({
    state: state.profile
})

export default connect(mapStateToProps)(Index);

{/* Progress bar */}
{/* <View style={styles.profileHeader.bottomWrapper}>
    <View style={styles.profileHeader.levelLabelWrapper}>
        <Text style={styles.profileHeader.levelLabel}>25</Text>
    </View>
    <Progress.Bar 
        progress={0.4}
        width={null}
        color="rgba(244,203,17,1)"
        unfilledColor="rgba(242,219,116,0.5)"
        useNativeDriver={true}
        />
</View> */}