import React from "react";
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    ScrollView,
    Modal,
    Button,
    Platform
} from "react-native";
// Importing connect from react-redux
import { connect } from "react-redux";
// Importing styles
import styles from "./style"

// Importing cooldown
import Cooldown from "../../Component/cooldown"
// Importing Icon from react-native-vector-icon
import Icon from "react-native-vector-icons/Ionicons";

import Quests from "../../tempQuests"
import { routeNames } from "../../Navigators/StackNavigator";


class Index extends React.Component{

    constructor(){
        super();
        this.state={
            modalVisibility: false,
            selectedQuest:{}
        }
    }
    static navigationOptions = {
        title: "Quest Board",
        headerTitleStyle :{
            fontWeight: "normal",
            color:"#2a2a2a"
        },
        tabBarLabel:"Quest Pool",
        tabBarIcon: <View style={{
                            justifyContent: Platform.OS === "ios" ? "center" : "flex-end"
                        }}>
                        <Icon 
                            name="ios-photos-outline"
                            size={25}
                            color="#2a2a2a"
                            />
                    </View>
    }

    //rotationQuest
    rotationQuest(item, index){
        return(
            <TouchableOpacity
                style={styles.quest.container}
                key={index}
                activeOpacity={0.8}
                onPress={() => {
                    //this.selectQuest(item)
                    this.props.navigation.navigate(routeNames.QuestViewer, {quest: item})
                }}
                >
                <View>
                    <Text style={styles.quest.titleText}>{item.title}</Text>
                    <Text style={styles.quest.categoryText}>Category: {item.category}</Text>
                    <Text style={styles.quest.rewardText}> Reward: {item.reward} </Text>
                </View>
            </TouchableOpacity>
        );
    }
    // When selecting a quest to read the details
    selectQuest(quest){
        this.setState({
            modalVisibility: true,
            selectedQuest: quest,
        })
    }
    // When finish reading the details of the quest and decides to close the modal
    deselectQuest(){
        this.setState({
            modalVisibility:false,
            selectedQuest:{}
        })
    }

    // Main render fucntion
    render(){
        return(
            <View style={styles.container.mainWindow}>
                <View style={styles.container.content}>
                    <View style={styles.container.cooldownContainer}>
                        <Cooldown />
                    </View>
                    
                    <FlatList
                        style={styles.container.questList}
                        data={Quests} 
                        keyExtractor={(index, item) => index}
                        renderItem={({index, item}) => this.rotationQuest(item, index)}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    state: state.questPool
})

export default connect(mapStateToProps)(Index);