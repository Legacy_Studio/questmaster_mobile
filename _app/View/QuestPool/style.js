import {
    StyleSheet
} from "react-native";
import colors from "../../Styles/colors"
export default {
    container:StyleSheet.create({
        mainWindow:{
            flex:1,
            backgroundColor:"#fff"
        },
        cooldownContainer:{
            paddingVertical:20,
        },
        content:{
            flex:1,
        },
        questList:{
            borderTopWidth:1,
            borderColor:"#2a2a2a",
            paddingTop:10,
            paddingBottom:30,
        }
    }),
    quest: StyleSheet.create({
        container:{
            borderWidth:1,
            borderColor:"#dddddd",
            marginHorizontal:20,
            marginTop:15,
            backgroundColor: colors.silver,
            padding:15,
        },
        titleText:{
            fontSize:16,
            textAlign:"left"
        },
        categoryText:{
            fontSize:10,
            textAlign:"left"
        },
        rewardText:{
            fontSize:12,
            textAlign:"left"
        }
    }),
    
} 