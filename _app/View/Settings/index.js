import React from "react";
import {
    View,
    Text,
    Platform
} from "react-native";
// Importing proptypes
import { PropTypes } from "prop-types";
// Importing vector icons
import Icon from "react-native-vector-icons/Ionicons";
// Facebook login integration
import FBSDK, {
    LoginButton,
    AccessToken,
    LoginManager
} from 'react-native-fbsdk';
// Importing Text Opacity Button
import TextButton from "../../Component/Button/TextButton";
// Importing styles
import styles from "./style"
// Importing the routes of the stack navigator
import { routeNames } from "../../Navigators/StackNavigator";
// Importing Stack navigator reducer actions
import navActions from "../../Service/Actions/StackNavigator";


class Index extends React.Component {

    // Navigation options
    static navigationOptions ={
        title: "Settings",
        headerTitleStyle :{
            fontWeight: "normal",
            color:"#2a2a2a"
        },
        tabBarLabel:"Settings",
        tabBarIcon: <View style={{
                            justifyContent: Platform.OS === "ios" ? "center" : "flex-end"
                        }}>
                        <Icon 
                            name="ios-settings-outline"
                            size={25}
                            color="#2a2a2a"
                            />
                    </View>
    }

    // Log out the user
    logOut(){
        LoginManager.logOut();
        this.props.navigation.dispatch({
            type: navActions.reset,
            routeName: routeNames.Login
        })
    }

    // Main render function
    render(){
        return(
            <View>
                <View style={styles.logOut.container}>
                    <TextButton 
                            style={styles.logOut.button}
                            function={() => this.logOut()}
                            textStyle={styles.logOut.text}
                            text="Log out"
                        />
                </View>
            </View>
        );
    }
}

Index.propTypes = {

}

export default Index;