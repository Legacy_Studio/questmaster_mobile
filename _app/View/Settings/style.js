import {
    StyleSheet,
    Dimensions
} from "react-native";
// Screen width and height
const { width, height } = Dimensions.get("window");
// Importing colors
import colors from "../../Styles/colors"

export default {
    containers: StyleSheet.create({
        base:{
            flex:1,
        }
    }),
    logOut: StyleSheet.create({
        container:{
            marginVertical:15,
            justifyContent:"center"
        },
        button:{
            alignSelf:"center",
            alignItems:"center",
            backgroundColor: colors.main,
            paddingHorizontal:20,
            paddingVertical:10,
            width:width*0.5
        },
        text:{
            color:"#2a2a2a"
        }
    }),
}