export const ApiNames = {
    FETCH_START: "FETCH_START",
    FETCH_SUCCESSFUL: "FETCH_SUCCESSFUL",
    FETCH_ERROR: "FETCH_ERROR",
};

const headerParams = {};

class Api {

    static setHeader(setHeader){
        if(setHeader){
            headerParams = setHeader;
        }
    }

    static getheader(){
        return headerParams;
    }

    //GET function
    static GET(route){
        return this.xhr(route, null, "GET");
    }

    //POST function
    static POST(route, params){
        return this.xhr(route, params, "POST");
    }
    //PATCH function
    static PATCH(route,params){
        return this.xhr(route,params, "PATCH")
    }
    // PUT function
    static PUT(route, params){
        let result = params ? params : null;
        console.log("PARAMS: ", result)
        return this.xhr(route, result, "PUT")
    }
    //UPDATE function
    static UPDATE(route, params){
        return this.xhr(route, params, "UPDATE");
    }
    
    //DELETE function
    static DELETE(route, params){
        return this.xhr(route, params, "DELETE");
    }

    // bodyBuilder
    static fetchBody(params){
        if(params){
            console.log("Params: ", params);
            if(params.type === "formData"){
                return  {body: params.form}
            }
            else{
                return  {body: JSON.stringify(params)}
            }
        }
        else{
            return null;
        }
            
    }
    //NORMAL fetching
    static xhr(route,params,verb, data){
        const host = "http://192.168.0.87:8080"; //"http://192.168.0.164:8080"
        const url = host + route;
                    
        let options = Object.assign(
            {method: verb},
            Api.fetchBody(params)
        );
        options.headers = Api.getheader();
        console.log("URL:", url)
        console.log("Options:", options)


        return fetch(url,options).then( resp => {
            let respJson = resp.json();
            if(resp.ok){
                return respJson;
            }
            return respJson.then( err => { return err});
        })
    }
}

export default Api;