import { NavigationActions } from "react-navigation";
import router from "../../Navigators";

const InitialState = router.StackNavigator.router.getStateForAction(NavigationActions.init());

// Navigate function
const NAVIGATE = (state, Method, routeName, params = {}) => {
    switch(Method){
      case "replace":
        return  NavigationActions.navigate({
                  routeName: routeName,
                  params: params,
                });
      case "reset":
        console.log("Here is the route name",routeName);
        return NavigationActions.reset({
                  index:0,
                  params: params,
                  actions: [NavigationActions.reset({routeName: routeName})]
                });
      case "back":
        return NavigationActions.back({
                  key: routeName
                });
      default: 
        return true;
    }
}

// Main reducer
const StackNavigatorReducer = (state = InitialState, action) => {
    //Checking if it's navigation.navigate action or not
    let actionControl = NAVIGATE(state, action.type, action.routeName);
    const nextState = actionControl === true ? 
                        router.StackNavigator.router.getStateForAction(action, state) 
                        : 
                        router.StackNavigator.router.getStateForAction(actionControl);
    return  nextState || state;
}

export default StackNavigatorReducer;