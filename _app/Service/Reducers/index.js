import { combineReducers } from "redux";

import StackNavigatorReducer from "./_StackNavigator";
import loginReducer from "./login";
import homeReducer from "./home";
import profileRedcuer from "./profile";
import questPoolReducer from "./questPool";
import AchievementReducer from "./Achievements";
import QuestLibraryReducer from "./questLibrary";
import PremiumQuestsReducer from "./questPremium";

export default combineReducers({
    Login: loginReducer,
    Home: homeReducer,
    profile: profileRedcuer,
    questPool: questPoolReducer,
    StackNavigator: StackNavigatorReducer,
    Achievements: AchievementReducer,
    QuestLibrary: QuestLibraryReducer,
    PremiumQuests: PremiumQuestsReducer
})