import {
    StyleSheet,
    Dimensions
} from "react-native";

const {height, width} = Dimensions.get("window");

export const defaultColor = "#2196F3";

export default StyleSheet.create({
    container:{
        flex:1,
    },
})