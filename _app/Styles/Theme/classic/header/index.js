import React from "react";
import {
    View
} from "react-native";
// Importing prop types
import PropTypes from "prop-types";
// Importing connect from react-redux
import { connect } from "react-redux";
// Importing styles
import styles, { defaultColor } from "./style"

class Index extends React.Component{
    render(){
        return(
            <View 
                style={[
                    styles.container,
                    {
                        backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : defaultColor
                    }
                ]}
            />
        );
    }
}

Index.propTypes = {
    backgroundColor: PropTypes.string
}

export default Index;